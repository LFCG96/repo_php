<?php
echo 'CLAUSULAS CONDICIONALES';

echo '<br><br>CLASULA IF<br>';
$edad = 16;

if ($edad < 18 ) {
  echo "No eres mayor de edad";
}
//----------------------------------------------------------------------
echo '<br><br>CLASULA IF ... ELSE<br>';

$edad2 = 20;

if ($edad2 < 18 ) {
  echo "No eres mayor de edad";
}
else {
	echo "Eres mayor de edad";
}
//-----------------------------------------------------------------------------------
echo '<br><br>CLASULA IF ... ELSEIF ... ELSE<br>';

$edad3 = 20;

if ($edad3 < 18 ) {
  echo "No eres mayor de edad";
}
elseif($edad3 == 20){
	echo "tienes 20 años";
}
else {
	echo "Eres mayor de edad";
}

//-----------------------------------------------------------------------------------
echo '<br><br>CLASULA SWITCH<br>';

$favcolor = "green";

switch ($favcolor) {
  case "red":
    echo "Your favorite color is red!";
    break;
  case "blue":
    echo "Your favorite color is blue!";
    break;
  case "green":
    echo "Your favorite color is green!";
    break;
  default:
    echo "Your favorite color is neither red, blue, or green!";
}

//--------------------------------------LOOPS
echo '<br><br>LOOP WHILE<br>';

$x = 10;

while($x <= 50) {
  echo "The number is: $x <br>"; //imprime la cadena the number es: y la variable
  $x+=10;
}

echo '<br>Segundo while , ahora x vale 60<br>';

while($x <= 100) {
  echo "The number is: $x <br>";
  $x+=10;
}

//---------------------------------------------------------------------------
echo '<br><br>LOOP DO WHILE<br>';

$y = 10;
do {
  echo "The number is: $y <br>";
  $y+=10;
} while ($y <= 50);

//----------------------------------------------------------------------
echo '<br><br>LOOP FOR<br>';

for ($contador = 10; $contador <= 20; $contador++) {
	echo "El contador vale: $contador <br>";
}

//-----------------------------------------------------------------------------
echo '<br><br>LOOP FOR EACH<br>';

$colores = array("rojo", "verde", "azul", "amarillo","negro");

foreach ($colores as $color) {
  echo "$color <br>";//Nota que ya no es necesario los putos para concatenar
}

//---------------------------------------------------------------------------
echo '<br><br>Clausula BREAK<br>';

for($cont1=0; $cont1 < 10; $cont1++){
	echo "el numero es: $cont1 <br>";
}
echo '<br>ya fuera del primer loop sin la clausula break<br>';

echo '<br>----------------<br><br>';

for ($cont = 0; $cont < 10; $cont++) {
  if ($cont == 5) {
    break;
  }
  echo "El número es: $cont <br>";
}
echo 'Ya fuera del loop con la clausula BREAK';

//---------------------------------------------------------------------------
echo '<br><br>Clausula CONTINUE<br>';

for($cont2=0; $cont2 < 10; $cont2++){
	echo "el numero es: $cont2 <br>";
}
echo '<br>ya fuera del primer loop sin la clausula CONTINUE<br>';

echo '<br>----------------<br><br>';

for ($cont2 = 0; $cont2 < 10; $cont2++) {
  if ($cont2 == 4) {
  	echo "<b>Omite el valor $cont2</b><br>";
    continue;
  }
  echo "El número es: $cont2 <br>";
}
echo 'Ya fuera del loop con la clausula CONTINUE';

//-------------------------------------------------------------------
echo '<br><br>WHILE CON BREAK Y CONTINUE<br>';

$cont3 = 0;

while($cont3 <= 10) {
	if($cont3 == 4 || $cont3 == 6) {
		$cont3++;
    	continue;
    }
    elseif($cont3 == 9){
		break;
    }
    echo "Contador vale: $cont3 <br>";
    $cont3++;
}

?>
