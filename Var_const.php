<?php

echo '<br>VARIABLES<br>';
$mi_variable = "Este texto esta en mi primera variable PHP<br>";
/*Las variables son CASE-Sensitive*/
$MI_VARIABLE = "Probando CASE-Sensitive<br>";
echo $mi_variable;
echo $MI_VARIABLE;

//No necesitamos definir el tipo de dato de las variables.
$nombre = "Luis";
$edad = 23;
echo "nombre: " . $nombre;
echo "<br>edad" . " ". $edad;

//--------------------------------------------------------CONSTANTES
echo '<br>';
echo '<br>CONSTANTES<br>';

define("MSG", "<br>Hola constante!"); //Definir una constante
echo MSG; //Imprimir

define("MSG", "<br>Hola desde una constante no sensible!", true); //es una constante no key sensitive
 echo msg;


//---------------ALCANCE
echo '<br>';
echo '<br>ALCANCE<br>';

function prueba(){
	$local="local";
	echo "variable dentro de la función: $local";
}

prueba(); //Se ejecuta la función

echo "variable fuera de la función: $local"; //Solo existe en la funcion
//Notice: Undefined variable: local in C:\xampp\htdocs\php\scripts\segundoscript.php on line 43


//-----------------------------------------------
$var_externa = "externa"; //Variable "global"

function prueba2(){
	echo "variable externa dentro de la función: $var_externa"; //Pero no se ve dentro D:
	//Notice: Undefined variable: var_externa in C:\xampp\htdocs\php\scripts\segundoscript.php on line 49
}

prueba2();

echo "variable externa fuera de la función: $var_externa";

//------------------------------------------------------------------------
//PALABRA RESERVADA GLOBAL
echo '<br>';
echo '<br>GLOBAL<br>';

$global = 'global';
$global2 = 2;
$global3 = 3;

function prueba3(){

	global $global; //utilizamos la palabra global para poder ver la variable externa
	$notglobal = 'noglobal';
	$global = $global.$notglobal;
	echo '<br>';
	echo $global;
}

function prueba4(){
	$GLOBALS['global2'] = $GLOBALS['global2'] + $GLOBALS['global3'];
}//Con $GLOBALS tienen visivilidad desde cualquier lugar

prueba3();

prueba4();
echo '<br>impresion prueba4<br>';
echo $global2;

//------------------------------------------STATIC
echo '<br>';
echo '<br> STATIC <br>';

function pruebaStatic() {
  static $x = 0; //Static es como en POO, guarda el valor, no se puede instanciar en un objeto
  echo 'dentro dela funcion'.$x;
  $x++;
}

pruebaStatic();
echo "<br>";
pruebaStatic();
echo "<br>";
pruebaStatic();

//------------------------------TIPOS DE DATOS
echo '<br>';
echo '<br>TIPOS DE DATOS<br>';

 //STRING
echo '<br>STRING<br>';
$string1 = "<br>----Hello world!"; //dobles comillas
$string2 = 'Hello world!';  //simples comillas

echo $string1.'<br>';
echo $string2.'<br>';
var_dump($string2); //También retorna la longitud o descripcion?
echo '<br>';

//---------------------------FUNCIONES STRING
echo '<br>FUNCIONES STRING<br>';
//String length
echo strlen("Hello world!").'<br>';
//string word count
echo str_word_count("Hello world!").'<br>';
//string position
echo strpos("Quiero saber la posicion donde inicia saber", "saber").'<br>';
//string replace
echo str_replace("world", "Dolly", "Hello world!").'<br>';

 //------------------------Integer
echo '<br>INTEGER<br>';
$int1 = 42; // positive
$int2 = -42; // negative

echo "<br>".$int1.'<br>';
echo 'usando var_dump'.var_dump($int2);
echo'<br>';
echo 'usando is_int '.var_dump(is_int($int1));
echo'<br>';
echo 'usando is_integer '.var_dump(is_integer($int2));
echo'<br>';
echo 'usando is_long '.var_dump(is_long($int2));

//----------------------------------------------------------------------------
//FLOAT
echo '<br><br>FLOAT<br>';
$float = 42.168;

echo "<br>".$float;
echo 'usando var_dump'.var_dump($float);
echo'<br>';
echo 'usando is_float '.var_dump(is_float($float));
echo'<br>';
echo 'usando is_double '.var_dump(is_double($float));

//-----------------------------Numerical strings
echo '<br><br>NUMERICAL STRINGS<br>';
$numstr = 5985;
var_dump(is_numeric($numstr));
echo'<br>';

$numstr2 = "5985";
var_dump(is_numeric($numstr2));
echo'<br>';

$numstr3 = "59.85" + 100;
echo $numstr3;
var_dump(is_numeric($numstr3));
echo'<br>';

$numstr4 = "Hello";
var_dump(is_numeric($numstr4));
echo'<br>';
//---------------------------------------------------------------------
//CAST FLOAT AND STRING TO INT
echo '<br><br>CAST FLOAT AND STRING TO INT<br>';

//Cast float to int
$float_to_cast = 23465.768;
$int_cast1 = (int)$float_to_cast;
echo $int_cast1;

echo "<br>";

// Cast string to int
$string_to_cast = "23465.768";
$int_cast2 = (int)$string_to_cast;
echo $int_cast2;

//----------------------------NUMBER FUNCTIONS
echo '<br><br>Number functions<br>';

//funcion pi
echo '<br>PI<br>';

echo(pi()); //Funciones se imprimen en ()
echo "<br>";

//funcion min and max()
echo '<br><br>MIN Y MAX<br>';
echo(min(0, 150, 30, 20, -8, -200));
echo "<br>";

echo(max(0, 150, 30, 20, -8, -200));
echo "<br>";

//funcion valor absoluto
echo '<br><br>Valor absoluto<br>';
echo(abs(-6.7));
echo "<br>";

//funcion raiz cuadrada
echo '<br><br>Raiz cuadrada<br>';
echo(sqrt(64));
echo "<br>";

//funcion redondear
echo '<br><br>Round<br>';
echo(round(0.60)); //Redondea al más cercano, sea siguiente o anterior
echo "<br>";

echo(round(0.49));
echo "<br>";

//funcion random
echo '<br><br>Random<br>';
echo(rand());
echo "<br>";

echo(rand(10, 20)); //Aleatorio en un rango
echo "<br>";

//--------------------------BOOLEAN
echo '<br><br>BOOLEAN<br>';
$true = true;
$false = false;

echo "<br>".$true; //Imprime 1
echo '<br>';
echo var_dump($false);//Con var dump si imprime la palabra

//--------------------------------NULL
echo '<br><br>NULL<br>';
echo '<br>';

$stringnotnull = "Hello world!";
$null = null;


var_dump($stringnotnull);
echo '<br>';
var_dump($null);

//--------------------------------------------------------------------------

$str = "20";
  $int = 35;
  $sum = $str + $int;
  echo "<br><br>Resultado de la suma de un string ".$str." y un integer ".$int." es igual a: ".$sum.'<br>';

?>
