
	var solicitudes_desplegado = 0;
	var mensajes_nuevos_desplegado = 0;
	var ventanas_abiertas = 0;
//Funcion para cargar las notificaciones
	function carga_notificaciones(){
		var peticion_solicitud_notificaciones = "php/ajax/solicitudes.php?tipo=notificaciones";
		var notificaciones = peticion_asincrona(peticion_solicitud_notificaciones).split("|");
		$("#contador_mensajes").html(notificaciones[0]);
		$("#contador_solicitudes").html(notificaciones[1]);
	}

//funciion para buscar usuario
	function buscar_usuarios(obj){
		var txt = $(obj).val();
		var peticion_usuarios = "php/ajax/listaUsuarios.php?busqueda=";

		if(txt.length >1 ){
			peticion_usuarios += "busqueda="+txt;	
		}	
		$("#listado_usuarios").html(peticion_asincrona(peticion_usuarios));
	}

//funcion para buscar contactos
	function buscar_contactos(obj){
		var txt = $(obj).val();
		var peticion_buscar_contactos = "php/ajax/contactos.php?busqueda=";

		if(txt.length >1 ){
			peticion_buscar_contactos += "busqueda="+txt;	
		}	
		$("#contactos_usuario").html( peticion_asincrona(peticion_buscar_contactos) );
	}

//Funcion para agregar solicitud de contacto
	function agregar_usuario(id_usuario){
		var peticion_solicitud = "php/ajax/solicitudes.php?tipo=enviar&id_dest=" + id_usuario;
		alert(peticion_asincrona(peticion_solicitud));
		location.reload();
	}

//funcion  para eliminar el contacto
	function elimina_contacto(id_contacto){
		if(!confirm("Realmente desesa eliminar contacto?")){
			return false;
		}
		var peticion_eliminar_contacto = "php/ajax/solicitudes.php?tipo=eliminar&id=" + id_contacto;
		alert(peticion_asincrona(peticion_eliminar_contacto));
		location.reload();
	}

//Funcion para listar solicitudes
	function muestra_lista_solicitudes(muestra){
		var peticion_lista_solicitudes = "php/ajax/solicitudes_usuarios.php";
		$("#detalle_solicitudes").html(peticion_asincrona(peticion_lista_solicitudes));
		//if(muestra == 1){
			if(solicitudes_desplegado == 1){
				solicitudes_desplegado = 0;
				$("#detalle_solicitudes").css("display" , "none");
				return false;
			}
			$("#detalle_solicitudes").css("display", "block");
			solicitudes_desplegado = 1;
			
			if(mensajes_nuevos_desplegado == 1){
				$("#detalle_mensajes").css("display", "none");
				mensajes_nuevos_desplegado = 0;
			}
	}

//funcion para ocultar la lista de mensajes
	function oculta_lista_mensajes(){
		$("#detalle_solicitudes").css("display", "none");
		solicitudes_desplegado = 0;
	}

//Funcion para mostrar la lista de mensajes
	function muestra_lista_mensajes(muestra){
		
		if(mensajes_nuevos_desplegado == 1){
			mensajes_nuevos_desplegado = 0;
			$("#detalle_mensajes").css("display" , "none");
			return false;
		}
		
		$("#detalle_mensajes").css("display", "block");
		solicitudes_desplegado = 1;
		var peticion_lista_solicitudes = "php/ajax/mensajes.php?tipo=lista_mensajes&id_contacto=";
		$("#detalle_mensajes").html(peticion_asincrona(peticion_lista_solicitudes));
		//if(muestra == 1){
		$("#detalle_mensajes").css("display", "block");
		mensajes_nuevos_desplegado = 1;

		if(solicitudes_desplegado == 1){
			$("#detalle_solicitudes").css("display", "none");
			solicitudes_desplegado = 0;
		}
	}

//funcion para repsonder la solicitud de contactos
	function responde_solicitud( id_solicitud , respuesta){
		var peticion_solicitud_respuesta = "php/ajax/solicitudes.php?tipo=responder&id=" + id_solicitud + "&resp=" + respuesta;
		alert(peticion_asincrona(peticion_solicitud_respuesta));
		location.reload();
	}

//Funcion que abre la ventana del chat
	function abre_chat(id){
	//busca si ya hay una ventana abierta
		if(document.getElementById("ventana_" + id)){
			$("#escritura_" + id).focus();
			return false;
		}
		if(ventanas_abiertas == 4){
			alert("No se pueden abrir mas de 4 ventanas! \nCierre otro chat para poder abrir esta conversacion.");
			return false;
		}
		ventanas_abiertas ++;
		var peticion_carga_chat = "php/ajax/ventana_chat.php?id_user=" + id;
		$("body").append(  peticion_asincrona(peticion_carga_chat) );
		$("#escritura_" + id).focus();
	}

//Funcion que minimiza la ventana del chat
	function minimiza_chat(id_contacto){
		$("#ventana_" + id_contacto).css("height" , "40px");
		$("#ventana_" + id_contacto).css("margin" , "48.5% 1% 0 0");
		$("#chat_" + id_contacto).css("display" , "none");
		$("#muestra_oculta_chat_" + id_contacto).attr("onclick" , "maximiza_chat(" + id_contacto + ");");
		$("#muestra_oculta_chat_" + id_contacto).html("+");
		$("#muestra_oculta_chat_" + id_contacto).addClass("verde");
	}
//funcion que maxiimiza la ventana del chat
	function maximiza_chat(id_contacto){
		$("#ventana_" + id_contacto).css("height" , "50%");
		$("#ventana_" + id_contacto).css("margin" , "25.5% 1% 0 0");
		$("#chat_" + id_contacto).css("display" , "block");		
		$("#muestra_oculta_chat_" + id_contacto).attr("onclick" , "minimiza_chat(" + id_contacto + ");");
		$("#muestra_oculta_chat_" + id_contacto).html("-");
		$("#muestra_oculta_chat_" + id_contacto).removeClass("verde");
	}

//Funcion para cerrar ventana de chat
	function cierra_chat(id_contacto){/*intervalo_busqueda, */
		$("#ventana_" + id_contacto).remove();
		ventanas_abiertas --;
	}
	
//Funcion para cargar conversacion en la ventana de chat
	function carga_mensajes(id_contacto){
		var peticion_mensajes = "php/ajax/mensajes.php?tipo=obtener&id_contacto=" + id_contacto;
		$("#contenido_chat_" + id_contacto).html( peticion_asincrona(peticion_mensajes) );
		//$("#contenido_chat_").scrollTop
		$('#contenido_chat_' + id_contacto).scrollTop( $('#contenido_chat_' + id_contacto).prop('scrollHeight') );


		$("#detalle_mensajes").css("display", "none");
	}

//Funcion para validar tecla enter en escritura
	function valida_tca_esc(e, id_contacto){
		if(e.keyCode == 13){
			enviar_mensaje(id_contacto);
		}
	}

//Funcion para guardar mendaje enviado en la BD
	function enviar_mensaje(id){
	//obtienen el valor de la caja de texto
		var mensaje = $("#escritura_" + id).val();
		if(mensaje.length <= 0){
			return false;
		} 
		var peticion_envia_mensaje = "php/ajax/mensajes.php?tipo=enviar&id_contacto=" + id + "&msg=" + mensaje;
		//alert(peticion_asincrona(peticion_envia_mensaje));
		peticion_asincrona(peticion_envia_mensaje);
		carga_mensajes(id);
		$("#escritura_" + id).val("");
	}

//Funcion para destruir la sesion del navegador
	function cerrar_sesion(){
		location.href="index.php?cierra=1";
	}

//Funcion para hacer peticiones asincronas (solo metodo GET)
	function peticion_asincrona(url){
        if(window.ActiveXObject){       
            var httpObj = new ActiveXObject("Microsoft.XMLHTTP");
        }else if (window.XMLHttpRequest){       
            var httpObj = new XMLHttpRequest(); 
        }
        httpObj.open("GET", url , false, "", "");
        httpObj.send(null);
        return httpObj.responseText;
    }


