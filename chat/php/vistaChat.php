<?php
	$usuario = $_SESSION['id_usuario'];//variable de sesion que contiene el id del usuario logueado
?>
<!-- Seccion de encabezado -->
<div class="header">
	<div>
		<img src="<?php echo $_SESSION['url_foto'];?>" width="100%" class="foto_usuario activo">

		<div class="cierra_sesion_banner" onclick="cerrar_sesion();">
			<img src="img/icono_cerra_sesion.jpg" class="imagen_cierra_sesion">
		</div>
		<div class="solicitudes">
			<img src="img/icono_mensaje.png" class="icono_solicitud" onclick="muestra_lista_mensajes(1);">
			<span class="numero_notificacion" id="contador_solicitudes">0</span>
			<div id="detalle_solicitudes" onmouseout=""> <!-- oculta_lista_solicitudes(); -->

				<?php 
					include('php/ajax/solicitudes_usuarios.php');//Incluye archivo que carga las solicitudes del usuario
				?>
			
			</div>
		</div>
		<div class="solicitudes">
			<img src="img/solicitud.jpg" class="icono_solicitud" onclick="muestra_lista_solicitudes(1);">
			<span class="numero_notificacion" id="contador_mensajes">0</span>
			<div id="detalle_mensajes" onblur="oculta_lista_mensaje();"></div>
		</div>
		<b class="nombre_usuario_header"><?php echo $_SESSION['nombre']; ?></b>
	</div>
</div>

<!-- Seccion de usuarios -->
<div class="busqueda_usuarios">
	<input type="text" class="buscador" onkeyup="buscar_usuarios(this);" placeholder="Buscar Usuarios...">
	<img src="img/icono_buscar.png" width="7%" height="20px;">

	<div id="listado_usuarios">
		<?php include('php/ajax/listaUsuarios.php'); ?>
	</div>
</div>

<!-- Seccion de contactos -->
<div class="busqueda_contactos">
	<input type="text" class="buscador" onkeyup="buscar_contactos(this);" placeholder="Buscar Contactos...">
	<img src="img/icono_buscar.png" width="7%" height="20px;">
</div>

<div id="contactos_usuario">
	<?php include('php/ajax/contactos.php');?>
</div>

<!-- Script para mandar peticiones asincronas que cargan las notificaciones -->
<script type="text/javascript">
	carga_notificaciones();//carga las notificaciones
	window.setInterval('carga_notificaciones()' , 6000);//actualiza las notificaciones en un intervalo de tiempo de 6 seg.
</script>
