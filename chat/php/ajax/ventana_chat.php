 <?php
 	session_start();
 	include('../conexion.php');
	$id_contacto = $_GET['id_user'];
	$id_usuario = $_SESSION['id_usuario'];
//consulta informacion de contacto
	$consulta = "SELECT
					CONCAT(nombre_usuario , ' (', alias, ')' ),
					foto
				FROM usuarios WHERE id_usuario = $id_contacto";
	$resultados = $conexion -> query($consulta) or die("Error al consultar los datos del contacto :" . $conexion -> error);
	$r = $resultados -> fetch_row();
	//			die($consulta);
?>

<div class="ventana_mensaje" id="<?php echo 'ventana_' . $id_contacto;?>">
	<div class="enc_msg">
		<img src="<?php echo $r[1];?>" class="foto_contacto_peq">
		<?php echo $r[0]; ?>

		<div class="boton_mini rojo" id="cierra_chat_<?php echo $id_contacto;?>" onclick="cierra_chat(<?php echo $id_contacto?>);">x</div>

		<div class="boton_mini amarillo" id="muestra_oculta_chat_<?php echo $id_contacto;?>" onclick="minimiza_chat(<?php echo $id_contacto;?>);">-</div>
	</div>

	<div id="<?php echo 'chat_' . $id_contacto;?>">
		<div class="contenido_chat" id="<?php echo 'contenido_chat_' . $id_contacto;?>"></div>
	
		<div class="escritura_chat">
			<textarea id="<?php echo 'escritura_' . $id_contacto;?>" class="escritura" onkeyup="valida_tca_esc(event,<?php echo $id_contacto;?>);"></textarea>
			<button class="enviar_msg" onclick="enviar_mensaje(<?php echo $id_contacto;?>);">
				<img src="img/icono_enviar.png" width="95%" height="30px">
			</button>
		</div>
	</div>

</div>

<!-- Script para carga los mensajes por cada ventana de chat-->
<script type="text/javascript"> 
//cargamos los mensajes
	carga_mensajes(<?php echo $id_contacto?>);
//creamos intervalo de tiempo para cargar los mensajes de la conversacion 
	setInterval("carga_mensajes(" + <?php echo $id_contacto; ?> +" );" , 5000);
</script>