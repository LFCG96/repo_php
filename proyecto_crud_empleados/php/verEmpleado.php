<?php
    include("conexion.php");

      $llave = $_GET['id'];
      $consulta = "SELECT * FROM emps WHERE id_emp=$llave";
      $ejecuta = $conexion->query($consulta) or die ("Error al consultar datos del empleado.");
      $emp = $ejecuta->fetch_row();
?>
<html>
<head>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.11.2/js/all.min.js" crossorigin="anonymous"></script>
</head>
<body>
<h2>Detalle Del Empleado</h2>
<br>
<a href="../Index.php"><i class="fas fa-share"></i> Regresar</a><br><br>
  <table>
    <tr>
      <td class="td_green">ID:</td>
      <td>
          <input class="texto_gris" type="number" value="<?php echo $emp[0]; ?>" readonly>
      </td>
    </tr>
    <tr>
      <td class="td_green">Nombres:</td>
      <td>
          <input class="texto_gris" type="text" value="<?php echo $emp[1]; ?>" readonly>
      </td>
    </tr>
    <tr>
      <td class="td_green">Apellidos:</td>
      <td>
          <input class="texto_gris" type="text" value="<?php echo $emp[2]; ?>" readonly>
      </td>
    </tr>
    <tr>
      <td class="td_green">Puesto:</td>
      <td>
          <input class="texto_gris" type="text" value="<?php echo $emp[4]; ?>" readonly>
      </td>
    </tr>
    <tr>
      <td class="td_green">Sueldo:</td>
      <td>
          <input class="texto_gris" type="number" value="<?php echo $emp[5]; ?>" readonly>
      </td>
    </tr>
    <tr>
      <td class="td_green">Dirección:</td>
      <td>
          <textarea class="texto_gris" readonly><?php echo $emp[3]; ?></textarea>
      </td>
    </tr>
  </table>
  <br>
  <br>
  <footer>
      <p style="color:grey; text-align:center;">&copy; 2020 - Luis Felipe Cabello Galicia</p>
  </footer>
</body>
</html>
<style type="text/css">
  h2,h3{color:rgba(150,150,150,.9);}
  th, .td_green{padding:10px; background:rgba(0, 128, 128,.5); border-radius:5px; color:white;}
  td{padding:7px; background:rgba(225,225,225,.5); border-radius:5px; color:grey;}
  button{background:rgba(250,250,250,1); border-color:rgba(225,0,0,0); color:rgba(0, 128, 128,.8); border-radius:5px;}
  button:hover, a:hover{background:rgba(225,225,225,.15);}
  button:focus,button:active, a:focus{color:rgba(0, 128, 128,.25); background: white;}
  .texto_gris{color:grey;}
  a{text-decoration: none; padding: 5px; background:rgba(250,250,250,1); border-color:rgba(225,0,0,0); color:rgba(0, 128, 128,.8); border-radius:5px; }
</style>
