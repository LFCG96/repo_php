<?php
    include("conexion.php");
    if(isset($_GET['id'])){
      $llave = $_GET['id'];
      $consulta = "SELECT * FROM emps WHERE id_emp=$llave";
      $ejecuta = $conexion->query($consulta) or die ("Error al consultar datos del empleado.");
      $emp = $ejecuta->fetch_row();
      echo "<h2>Editar Datos De Empleado</h2>";
    }else{
      $emp[0]="";
      $emp[1]="";
      $emp[2]="";
      $emp[3]="";
      $emp[4]="";
      $emp[5]="";
      echo "<h2>Nuevo Empleado</h2>";
    }
?>
<html>
<head>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.11.2/js/all.min.js" crossorigin="anonymous"></script>
</head>
<body>
  <a href="../Index.php"><i class="fas fa-share"></i> Regresar</a>
<br><br>
<form action="guardaEdicion.php" method="post">
  <table>
    <tr>
      <td class="td_green">ID:</td>
      <td>
          <input class="texto_gris" type="number" name="id_emp" value="<?php echo $emp[0]; ?>" readonly>
      </td>
    </tr>
    <tr>
      <td class="td_green">Nombres:</td>
      <td>
          <input class="texto_gris" type="text" name="nom_emp" value="<?php echo $emp[1]; ?>">
      </td>
    </tr>
    <tr>
      <td class="td_green">Apellidos:</td>
      <td>
          <input class="texto_gris" type="text" name="ap_emp" value="<?php echo $emp[2]; ?>">
      </td>
    </tr>
    <tr>
      <td class="td_green">Puesto:</td>
      <td>
          <input class="texto_gris" type="text" name="puesto" value="<?php echo $emp[4]; ?>">
      </td>
    </tr>
    <tr>
      <td class="td_green">Sueldo:</td>
      <td>
          <input class="texto_gris" type="number" name="sueldo" value="<?php echo $emp[5]; ?>">
      </td>
    </tr>
    <tr>
      <td class="td_green">Dirección:</td>
      <td>
          <textarea class="texto_gris" name="direccion"><?php echo $emp[3]; ?></textarea>
      </td>
    </tr>
    <tr>
      <td colspan="2" align="center">
        <button type="submit"><i class="fas fa-save"></i> Guardar</button>
      </td>
    </tr>
  </table>
</form>
<br><br>
<footer>
    <p style="color:grey; text-align:center;">&copy; 2020 - Luis Felipe Cabello Galicia</p>
</footer>
</body>
</html>

<style type="text/css">
  h2,h3{color:rgba(150,150,150,.9);}
  th, .td_green{padding:10px; background:rgba(0, 128, 128,.5); border-radius:5px; color:white;}
  td{padding:7px; background:rgba(225,225,225,.5); border-radius:5px; color:grey;}
  button{background:rgba(250,250,250,1); border-color:rgba(225,0,0,0); color:rgba(0, 128, 128,.8); border-radius:5px;}
  button:hover, a:hover{background:rgba(225,225,225,.15);}
  button:focus,button:active, a:focus{color:rgba(0, 128, 128,.25); background: white;}
  .texto_gris{color:grey;}
  a{text-decoration: none; padding: 5px; background:rgba(250,250,250,1); border-color:rgba(225,0,0,0); color:rgba(0, 128, 128,.8); border-radius:5px; }
</style>
