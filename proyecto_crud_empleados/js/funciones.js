
function nuevoEmp(){//Esta funcion abre la vista de edicion/nuevo
  window.location="php/edicionEmpleado.php";
}

function editEmp(id_emp){//Esta funcion abre la vista de edicion/nuevo
  window.location="php/edicionEmpleado.php?id="+id_emp;
}

function leerEmp(id_emp){//Esta funcion abre la vista para ver el detalle
  window.location="php/verEmpleado.php?id="+id_emp;
}

function eliminarEmp(id_emp){ //Esta funcion usa ajax para eliminar un empleado de manera asíncrona
    $.ajax({
      type: 'POST',
      url: 'php/eliminarEmp.php',
      data: {id: id_emp},
      cache: false,
      success: function(respuesta){//Imprimimos lo que retorne el php por medio del echo
          alert(respuesta);
          location.reload();//Recargamos la página
      }
    });
}
