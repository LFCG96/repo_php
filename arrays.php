<?php
//arrays
echo '<br>ARRAYS INDEXADOS<br>';

$carros = array("Volvo","BMW","Toyota","Ferrari");//Definimos el array
var_dump($carros); //var_dump retorna el tipo de variable y los datos
echo "<br>" . $carros[0] . ", " . $carros[1] . " y " . $carros[2] . ".";

//---------------funcion count()
echo '<br><br>Funcion COUNT()<br>';
echo '<br> El array $carros tiene: '.count($carros). ' elementos';

//---------------------------------------------------------------------------
echo '<br><br>ARRAYS ASOCIATIVOS<br>';

//en vez de indices se accedera por las claves dadas antes de la flecha
$age = array("Peter"=>"35", "Ben"=>"37", "Joe"=>"43");

//otra froma de definirlo...
$age['Miguel'] = "12";
$age['Alex'] = "24";
$age['Daniela'] = "45";

//accedemos a cada elemento mediante su llave
echo "Peter tiene " . $age['Peter'] . " años";
echo "<br>Ben tiene " . $age['Ben'] . " años";
echo "<br>Joe tiene " . $age['Joe'] . " años";
echo "<br>Miguel tiene " . $age['Miguel'] . " años";
echo "<br>Alex tiene " . $age['Alex'] . " años";
echo "<br>Daniela tiene " . $age['Daniela'] . " años";

//------------------------------------------------------------------
echo '<br><br>ARRAYS MULTIDIMENSIONALES<br>';
//Es un array compuesto de otros arrays
$carros2 = array (
  array("Volvo",22,18),
  array("BMW",15,13),
  array("Saab",5,2),
  array("Land Rover",17,15)
);

//accdemos a cada elemento de los arreglos mediante sus indices

echo $carros2[0][0].": stock: ".$carros2[0][1].", vendidos: ".$carros2[0][2].".<br>";
echo $carros2[1][0].": stock: ".$carros2[1][1].", vendidos: ".$carros2[1][2].".<br>";
echo $carros2[2][0].": stock: ".$carros2[2][1].", vendidos: ".$carros2[2][2].".<br>";
echo $carros2[3][0].": stock: ".$carros2[3][1].", vendidos: ".$carros2[3][2].".<br>";

//------------------------------------------------------------------------------
echo '<br><br>FOR LOOPS<br>';

$cars = array("Volvo", "BMW", "Toyota");

for($i = 0; $i < count($cars); $i++) {
  echo '<br>'.$cars[$i];
}

//-----------------------------------------------------------------------------
echo '<br><br>FOR EACH LOOP ARRAY ASOCIATIVOS<br>';

$lista_edades = array("Peter"=>"35", "Ben"=>"37", "Joe"=>"43");

//Para cada elemento del array $lsita_edades, con el formato $clave,$valor
foreach($lista_edades as $clave => $valor) {
  echo "Clave=" . $clave . ", Valor=" . $valor;
  echo "<br>";
}

//----------------------------------------------------------------------------
echo '<br><br>FOR EACH LOOP ARRAY MULTIDIMENSIONALES<br>';

$carros3 = array ( //array de arrays :D
  array("Volvo",22,18),
  array("BMW",15,13),
  array("Saab",5,2),
  array("Land Rover",17,15)
);

//CICLO FOR PARA RECORRER EL ARRAY PRINCIPAL
for ($array_interno = 0; $array_interno < 4; $array_interno++) {

  echo "<p><b>Array interno: $array_interno</b></p>";
  echo "<ul>";

  //EL SEGUNDO FOR RECORRERA LOS ELEMENTOS DE CADA ARRAY INTERNO
  for ($info_array_interno = 0; $info_array_interno < 3; $info_array_interno++) {
    echo "<li>".$carros3[$array_interno][$info_array_interno]."</li>";
  }
  echo "</ul>";
}

//UTILIZANDO LA LONGITUD VAIRABLE DE LOS ARRAY
echo '<br>Podemos obtener los limites de la iteracion con la funcion count()<br>';

$carros4 = array (
  array("Volvo",22,18),
  array("BMW",15,13),
  array("Saab",5,2),
  array("Land Rover",17,15)
);


for ($array_interno = 0; $array_interno < count($carros4); $array_interno++) {
  echo "<p><b>Array interno: $array_interno</b></p>";
  echo "<ul>";
  for ($info_array_interno = 0; $info_array_interno < count($carros4[$array_interno]); $info_array_interno++) {
    echo "<li>".$carros4[$array_interno][$info_array_interno]."</li>";
  }
  echo "</ul>";
}


?>
