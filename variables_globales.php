<?php
//VARIABLES SUPERGLOBALES
//$GLOBALS
echo '<br>VARIABLE $GLOBALS<br><br>';
$x = 75;
$y = 25;

echo 'TIPO DE DATO DE $GLOBALS:<br> ';
var_dump($GLOBALS);

function addition() {
  $GLOBALS['z'] = $GLOBALS['x'] + $GLOBALS['y'];//Define z y le asigna la suma
  echo '<br><br>';
}

addition();
echo 'El valor de z es: '.$z;

//--------------------------------------------------------------------------------

//$_SERVER muestra  datos del servidor que está ejecutandose
echo '<br><br>VARIABLE $_SERVER<br><br>';

echo 'PHP_SELF: '.$_SERVER['PHP_SELF'];
echo "<br>";
echo 'SERVER_NAME: '.$_SERVER['SERVER_NAME'];
echo "<br>";
echo 'HTTP_POST: '.$_SERVER['HTTP_HOST'];
echo "<br>";
echo 'HTTP_USER_AGENT: '.$_SERVER['HTTP_USER_AGENT'];
echo "<br>";
echo 'SCRIPT_NAME: '.$_SERVER['SCRIPT_NAME'];

//----------------------------------------------------------
echo '<br><br>VARIABLE $_REQUEST<br><br>';

echo '
<form method="post" action=""?>
  Nombre: <input type="text" name="nombre_formulario">
  <input type="submit">
</form>
'; //Crea un form post que recupera nombre_formulario

if ($_SERVER["REQUEST_METHOD"] == "POST") {
    $nombre_formulario = htmlspecialchars($_REQUEST["nombre_formulario"]);//La recuperamos
    if (empty($nombre_formulario)) {
        echo "El campo nombre esta vacio";
    } else {
        echo $nombre_formulario;
    }
}

//-----------------------------------------------------------------------------
echo '<br><br>VARIABLE $_POST<br><br>';

echo '
<form method="post" action=""?>
  Nombre: <input type="text" name="nombre_formulario">
  <input type="submit">
</form>
';

if ($_SERVER["REQUEST_METHOD"] == "POST") {
    $nombre_formulario = $_POST['nombre_formulario'];
    if (empty($nombre_formulario)) {
        echo "El campo nombre esta vacio";
    } else {
        echo $nombre_formulario;
    }
}

//------------------------------------------------------
echo '<br><br>VARIABLE $_GET<br><br>';

echo '<a href="variables_globales.php?subject=PHP&web=BecaEveris">Test $GET</a>';

echo "<br>Estudiando " . $_GET['subject'] . " en " . $_GET['web'];


?>
