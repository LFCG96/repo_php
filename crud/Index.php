<?php

?>

<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title>CRUD - INDEX</title>
    <script type="text/javascript" src="js/jquery-3.5.1.min.js"></script>
    <script type="text/javascript" src="js/funciones.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.11.2/js/all.min.js" crossorigin="anonymous"></script>
  </head>
  <body>
    <h2>CRUD PRODUCTOS</h2>
    <br>
    <button type="button" onclick="abre_nuevo_form();"><i class="fas fa-file-medical"></i> Agregar</button>
    <br><br>
    <div id="contenido_lista">
      <?php include("php/listadoProductos.php"); ?>
    </div>
    <br>
    <br>
    <h3>Por: Luis Felipe Cabello Galicia</h3>
  </body>
</html>


<style type="text/css">
  h2,h3{color:rgba(150,150,150,.9);}
  th{padding:10px; background:rgba(225,0,0,.5); border-radius:5px; color:white;}
  td{padding:7px; background:rgba(225,225,225,.5); border-radius:5px; color:grey;}
  button{background:rgba(250,250,250,1);; border-color:rgba(225,0,0,0); color:rgba(225,0,0,.8); border-radius:5px;}
  button:hover{background:rgba(225,225,225,.15);}
  button:focus,button:active{color:rgba(225,0,0,.25);background: white;}
</style>
