
function formProducto(id_producto){
  window.location="php/edicionProducto.php?id="+id_producto;
}

function abre_nuevo_form(){
  window.location="php/edicionProducto.php";
}

function leerProducto(id_producto){
  window.location="php/verProducto.php?id="+id_producto;
}

function eliminarProducto(id_producto){
    $.ajax({
      type: 'POST',
      url: 'php/eliminarProducto.php',
      data: {id: id_producto},
      cache: false,
      success: function(mi_respuesta){
          alert(mi_respuesta);
          location.reload();
      }
    });
}
