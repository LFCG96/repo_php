<?php
    include("conexion.php");

    $consulta = "SELECT id_prod,nombre,marca,precio,observaciones FROM prods";

    $lista = $conexion->query($consulta) or die("Error al consultar lista de productos: <br>".$conexion->error);
?>

<table id="lista_productos">
    <tr>
        <th>ID</th>
        <th>Nombre</th>
        <th>Marca</th>
        <th>Precio</th>
        <th>Observaciones</th>
        <th colspan="3">Opciones</th>
    </tr>
    <?php
        while ($arr_product = $lista->fetch_row() ) {
          echo "<tr>";
          echo "<td>". $arr_product[0] ."</td>";
          echo "<td>". $arr_product[1] ."</td>";
          echo "<td>". $arr_product[2] ."</td>";
          echo "<td>$". $arr_product[3] ."</td>";
          echo "<td>". $arr_product[4] ."</td>";
          echo '<td><button type="button" onclick="formProducto('.$arr_product[0].');"><i class="fas fa-edit"></i></button></td>';
          echo '<td><button type="button" onclick="leerProducto('.$arr_product[0].');"><i class="fas fa-tasks"></i></button></td>';
          echo '<td><button type="button" onclick="eliminarProducto('.$arr_product[0].');"><i class="	fas fa-skull-crossbones"></i></button></td>';
          echo "</tr>";
        }
    ?>
</table>
