<?php
    include("conexion.php");
    if(isset($_GET['id'])){
      $llave = $_GET['id'];
      $consulta = "SELECT id_prod,nombre,precio,marca,observaciones FROM prods WHERE id_prod=$llave";
      $ejecuta = $conexion->query($consulta) or die ("Error al consultar datos del producto.");
      $prod = $ejecuta->fetch_row();
      echo "<h2>Editar Producto</h2>";
    }else{
      $prod[0]="";
      $prod[1]="";
      $prod[2]="";
      $prod[3]="";
      $prod[4]="";
      echo "<h2>Nuevo Producto</h2>";
    }
?>
<html>
<head>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.11.2/js/all.min.js" crossorigin="anonymous"></script>
</head>
<body>
  <a href="../Index.php"><i class="fas fa-share"></i> Regresar</a>
<br><br>
<form action="guardaEdicion.php" method="post">
  <table>
    <tr>
      <td class="td_red">ID:</td>
      <td>
          <input class="texto_gris" type="number" name="id_prod" value="<?php echo $prod[0]; ?>" readonly>
      </td>
    </tr>
    <tr>
      <td class="td_red">Nombre:</td>
      <td>
          <input class="texto_gris" type="text" name="nom_prod" value="<?php echo $prod[1]; ?>">
      </td>
    </tr>
    <tr>
      <td class="td_red">Precio:</td>
      <td>
          <input class="texto_gris" type="number" name="precio" value="<?php echo $prod[2]; ?>">
      </td>
    </tr>
    <tr>
      <td class="td_red">Marca:</td>
      <td>
          <input class="texto_gris" type="text" name="marca" value="<?php echo $prod[3]; ?>">
      </td>
    </tr>
    <tr>
      <td class="td_red">Observaciones:</td>
      <td>
          <textarea class="texto_gris" name="observaciones"><?php echo $prod[4]; ?></textarea>
      </td>
    </tr>
    <tr>
      <td colspan="2" align="center">
        <button type="submit"><i class="fas fa-save"></i> Guardar</button>
      </td>
    </tr>
  </table>
</form>

<br><br><h3>Por: Luis Felipe Cabello Galicia</h3>
</body>
</html>
<style type="text/css">
  h2,h3{color:rgba(150,150,150,.9);}
  th, .td_red{padding:10px; background:rgba(225,0,0,.5); border-radius:5px; color:white;}
  td{padding:7px; background:rgba(225,225,225,.5); border-radius:5px; color:grey;}
  button{background:rgba(250,250,250,1);; border-color:rgba(225,0,0,0); color:rgba(225,0,0,.8); border-radius:5px;}
  button:hover, a:hover{background:rgba(225,225,225,.15);}
  button:focus,button:active, a:focus{color:rgba(225,0,0,.25);background: white;}
  .texto_gris{color:grey;}
  a{text-decoration: none; padding: 5px; background:rgba(250,250,250,1);; border-color:rgba(225,0,0,0); color:rgba(225,0,0,.8); border-radius:5px; }
</style>
